@extends('main')

@section('content')
    <section id="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h2>{{ $page_header }}</h2>
            <form method="get"  action="{{Request::url()}}" >
                <div class="form-group">
                    <label for="Select2">Авторы</label>
                    <select multiple class="form-control" id="search_authors" name="authors[]">
                        @foreach($authors as $author)
                        
                            @if(count(app('request')->input('authors')) > 0 && in_array($author->id, app('request')->input('authors')))
                                <option value="{{ $author->id }}" selected>{{ $author->name }} {{ $author->surname }}</option>
                            @else
                                <option value="{{ $author->id }}">{{ $author->name }} {{ $author->surname }}</option>
                            @endif
                            
                        @endforeach
                    </select>
                </div>
            </form>
            
            <table class="table">
                <thead>
                  <tr>
                    <th style="width: 50%">Книга</th>
                    <th>Авторы</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($books as $book)
                        <tr>
                            <td>{{ $book['name'] }}</td>
                            <td>
                                @if(array_key_exists('authors', $book))
                                    {{ implode(', ', $book['authors']) }}
                                @else
                                    {{ 'Нет авторов' }}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
          </div>
        </div>
      </div>
    </section>
@endsection