@extends('main')

@section('content')
    <section id="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h2>{{ $page_header }}</h2>
            <table class="table">
                <thead>
                  <tr>
                    <th style="width: 50%">Автор</th>
                    <th>Книги</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($authors as $author)
                        <tr>
                            <td>{{ $author['name'] }}</td>
                            <td>
                                @if(array_key_exists('books', $author))
                                    {{ implode(', ', $author['books']) }}
                                @else
                                    {{ 'Нет книг' }}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
          </div>
        </div>
      </div>
    </section>
@endsection