<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Библиотека</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ URL::asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ URL::asset('css/scrolling-nav.css') }}" rel="stylesheet">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="{{ URL::to('/') }}">Библиотека</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ URL::to('/books') }}">Книги</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ URL::to('/authors') }}">Авторы</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ URL::to('/new_book') }}">Добавить книгу</a>
            </li>
			<li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ URL::to('/new_author') }}">Добавить автора</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    
    @yield('content')
    
    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
		<p class="m-0 text-center text-white"> &copy; 2017</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="{{ URL::asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('vendor/popper/popper.min.js') }}"></script>
    <script src="{{ URL::asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>

    <!-- Plugin JavaScript -->
    <script src="{{ URL::asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.full.min.js') }}"></script>
    
    
    <!-- Custom JavaScript for this theme -->
    <script src="{{ URL::asset('js/scrolling-nav.js') }}"></script>
    <script>
    
        $(document).ready(function(){
            $("#Select2").select2({});
            $('#search_authors').select2({})
        });
        
        $(document).on('submit', '#add_author', function(e){
            e.preventDefault(e);
            $.ajax({
                type:"POST",
                url: $(this).attr('action'),
                data:$(this).serialize(),
                dataType: 'json',
                success: function(data){
                    alert('Автор успешно добавлен');
                    window.location.reload();
                },
                error: function(data){
                    alert('Произошла ошибка');
                }
            });
        });
        
        $(document).on('submit', '#add_book', function(e){
            e.preventDefault(e);
            $.ajax({
                type:"POST",
                url: $(this).attr('action'),
                data:$(this).serialize(),
                dataType: 'json',
                success: function(data){
                    alert('Книга успешно добавлена');
                    window.location.reload();
                },
                error: function(data){
                    alert('Произошла ошибка');
                }
            });
        });
        
        $(document).on('change', '#search_authors', function(){
            $(this).closest('form').trigger('submit');
        });
        
    </script>
  </body>

</html>