@extends('main')

@section('content')
    <section id="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h2>{{ $page_header }}</h2>
                <form method="post" id="add_author" action="{{Request::url()}}/save">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="InputName">Имя</label>
                        <input type="text" class="form-control" name="name" id="InputName" placeholder="Имя автора" required />
                        <small id="emailHelp" class="form-text text-muted">Можно добавить отчество</small>
                    </div>
                    <div class="form-group">
                        <label for="InputName">Фамилия</label>
                        <input type="text" class="form-control" name="surname" id="InputSurname" placeholder="Фамилия автора" required />
                    </div>
                    <div class="form-group">
                        <label for="Select2">Книги</label>
                        <select multiple class="form-control" id="Select2" name="books[]">
                            @foreach($books as $book)
                                <option value="{{$book->id}}">{{$book->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Добавить автора</button>
                </form>
          </div>
        </div>
      </div>
    </section>
@endsection