@extends('main')

@section('content')
    <section id="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h2>{{ $page_header }}</h2>
                <form method="post" id="add_book" action="{{Request::url()}}/save">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="InputName">Имя</label>
                        <input type="text" class="form-control" name="name" id="InputName" placeholder="Название книги" required />
                    </div>
                    <div class="form-group">
                        <label for="Select2">Авторы</label>
                        <select multiple class="form-control" id="Select2" name="authors[]">
                            @foreach($authors as $author)
                                <option value="{{ $author->id }}">{{ $author->name }} {{ $author->surname }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Добавить автора</button>
                </form>
          </div>
        </div>
      </div>
    </section>
@endsection