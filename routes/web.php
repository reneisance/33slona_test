<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BooksController@list');
Route::get('/books', 'BooksController@list');
Route::get('/authors', 'AuthorsController@list');

Route::get('/new_author', 'AuthorsController@new');
Route::post('/new_author/save', 'AuthorsController@save');

Route::get('/new_book', 'BooksController@new');
Route::post('/new_book/save', 'BooksController@save');