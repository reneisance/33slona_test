<?php
namespace App;

use App\Author;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = ['name'];
    public $timestamps = false;
    
    public function authors(){
        
        return $this->belongsToMany('App\Author', 'links', 'book_id', 'author_id' );
    
    }
    
    public static function render_data($book = null){
        
        if(is_null($book)){
            return [];
        }
        
        $output_books = [];
        foreach($book as $item){
            $book_data = [];
            $book_data['name'] = $item->name;
            $book_data['authors'] = [];
            $book_authors = $item->authors()->get();
            if(!is_null($book_authors)){
                foreach($book_authors as $author){
                    $book_data['authors'][] = $author->name . ' ' . $author->surname;
                }    
            }
            array_push($output_books, $book_data);
        }
        
        return $output_books;
        
    }
    
    public static function set_authors($book_id, $authors){
        
        if(is_array($authors)){
            if(count($authors) > 0){
                $new_links = [];
                foreach($authors as $author){
                    $new_links[] = [
                        'book_id' => $book_id,
                        'author_id' => $author
                    ];
                }
                DB::table('links')->insert($new_links);
            }
        }
        
        return;
        
    }
    
}
