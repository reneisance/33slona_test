<?php
namespace App\Http\Controllers;

use App\Author;
use App\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AuthorsController extends Controller
{
    
    public function list(){
        
        $authors = Author::all();
       
        return view('authors_list',[
            'page_header' => 'Список авторов',
            'authors' => Author::render_data($authors)
        ]);
        
    }
    
    public function new(Request $request){
        
        return view('new_author', [
            'page_header' => 'Добавление нового автора',
            'books' => Book::all()
        ]);
        
    }
    
    public function save(Request $request){
        
        $this->validate($request, [
            'name' => 'required',
            'surname' => 'required'
        ]);
        
        $author = new Author();
        $author->name = $request->name;
        $author->surname = $request->surname;
        $author->save();
        
        if(!is_null($author->id)){
            Author::set_books($author->id, $request->books);
        }
        
        return response()->json(['responseText' => 'success!'], 200);
        
    }
    
}
