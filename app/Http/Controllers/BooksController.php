<?php
namespace App\Http\Controllers;

use App\Author;
use App\Book;
use Illuminate\Http\Request;

class BooksController extends Controller
{
    
    public function list(Request $request){
        
        if(isset($request->authors)){
            $authors = Author::find($request->authors);
            $books = $this->get_books($authors);
            $books = $this->get_unique($books);
        }else{
            $books = Book::all();
        }
        
        return view('books_list',[
            'page_header' =>'Список книг',
            'books' => Book::render_data($books),
            'authors' => Author::all()
        ]);
        
    }
    
    public function new(Request $request){
        
        return view('new_book', [
            'page_header' => 'Добавление новой книги',
            'authors' => Author::all()
        ]);
        
    }
    
    public function save(Request $request){
        
        $this->validate($request, [
            'name' => 'required'
        ]);
        
        $book = new Book();
        $book->name = $request->name;
        $book->save();
        
        if(!is_null($book->id)){
            Book::set_authors($book->id, $request->authors);
        }
        
        return response()->json(['response' => 'success'], 200);
        
    }
    
    private function get_unique($books){
        
        $response = [];
        foreach($books as $key => $book){
            if(!in_array($book->id, $response)){
                $response[] = $book->id;
            }else{
                unset($books[$key]);
            }
        }
        
        sort($books);
        
        return $books;
    }
    
    private function get_books($authors){
        
        $books = [];
        foreach($authors as $author){
            foreach($author->books()->get() as $book){
                $books[] = $book;   
            }
        }
        
        return $books;
    }
    
}
