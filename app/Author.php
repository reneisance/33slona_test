<?php
namespace App;

use App\Book;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    protected $fillable = ['name'];
    public $timestamps = false;
    
    public function books(){
        
        return $this->belongsToMany('App\Book', 'links', 'author_id', 'book_id');
    
    }
    
    public static function render_data($author = null){
        
        if(is_null($author)){
            return [];
        }
        
        $output_authors = [];
        foreach($author as $item){
            $author_data = [];
            $author_data['name'] = $item->name . ' ' . $item->surname;
            $author_data['authors'] = [];
            $author_books = $item->books()->get();
            if(!is_null($author_books)){
                foreach($author_books as $book){
                    $author_data['books'][] = $book->name . ' ' . $book->surname;
                }
            }
            array_push($output_authors, $author_data);
        }
        
        return $output_authors;
        
    }
    
    public static function set_books($author_id, $books){
        
        if(is_array($books)){
            if(count($books) > 0){
                
                $new_links = [];
                foreach($books as $book){
                    $new_links[] = [
                        'author_id' => $author_id,
                        'book_id' => $book
                    ];
                }
                DB::table('links')->insert($new_links);
            }
        }
        
        return;
        
    }
    
}
